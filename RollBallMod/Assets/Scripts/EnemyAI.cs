using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//https://low-scope.com/unity-quick-the-most-common-ways-to-move-a-object/#:~:text=The%20most%20common%20way%20of,also%20possible%20to%20call%20transform.
public class EnemyAI : MonoBehaviour
{
    private bool movingLeft;

    [SerializeField] private Vector3 pointA = new Vector3(-10, 2, 20);
    [SerializeField] private Vector3 pointB = new Vector3(10, 2, 20);
    [SerializeField] private float speed = 1;
    private float t;
    private void Update()
    {
        t += Time.deltaTime * speed;
        // Moves the object to target position
        transform.position = Vector3.Lerp(pointA, pointB, t);
        // Flip the points once it has reached the target
        if (t >= 1)
        {
        var b = pointB;
        var a = pointA;
        pointA = b;
        pointB = a;
        t = 0;
        }
    }
    // What Linear interpolation actually looks like in terms of code
    private Vector3 CustomLerp(Vector3 a, Vector3 b, float t)
    {
        return a * (1 - t) + b * t;
    }
}