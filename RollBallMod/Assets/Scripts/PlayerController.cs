using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    //public TextMeshProUGUI timer;

    public float speed = 0;
    private Rigidbody rb;
    private float movementX;
    private float movementY;

    private float jumps = 3;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    private void OnJump(InputValue movementValue)
    {
        //https://www.youtube.com/watch?v=Yjee_e4fICc
        jumps -= 1;
        if (jumps > 0)
        {
            rb.AddForce(Vector3.up * 6f, ForceMode.Impulse);
        }
    }

    void OnTriggerEnter(Collider other) 
	{
        if (other.gameObject.CompareTag ("enemy"))
		{
            gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * speed);
    }

    //Debug.Log("")

}